#!/bin/bash

FOLDER=$3
ARCHIVE=${FOLDER}.zip

echo $3
echo $ARCHIVE

zip -r $ARCHIVE $3

curl -T $ARCHIVE --user "$1:$2" -sw '%{http_code}' https://polybox.ethz.ch/remote.php/dav/files/$1/
FAILED=$?

echo
if test $FAILED = "0"; then
    echo upload done
else
    echo upload failed
fi
