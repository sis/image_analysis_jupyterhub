# Installation

1. Clone GitHub repositories:
```
mkdir image_analysis_tools
cd image_analysis_tools

git clone git@github.com:Microbial-Systems-Ecology/midap.git
git clone git@gitlab.ethz.ch:sis/image_analysis_jupyterhub.git
```

2. Navigate to folder:
```
cd midap
```

3. Create conda env (or virtual env using Python 3.10) and install midap:
```
conda create --name midap_jupyter python=3.10
conda activate midap_jupyter
pip install -e .
```

4. Trigger download of model weights:
```
midap_download
```

5. For further installation navigate to folder containing notebooks:
```
cd ..
cd image_analysis_jupyterhub
```

6. Installation of packages required by notebooks:
```
pip install -r requirements.txt
```
